#!/usr/bin/env python3

import json
import sys

def print_text_position(text, pos, length=1):
    text = text.replace("\t", " ")

    line_index = text[:pos].count('\n')
    lines = text.split('\n')

    if line_index > 0:
        print(f" {line_index} | {lines[line_index - 1]}")
    prefix = f" {line_index + 1} | "
    print(prefix + lines[line_index])

    # pointer to pos
    column_index = pos - sum((len(line) + 1) for line in lines[:line_index]) - 1
    print((len(prefix) - 3) * " ", "|",  column_index * " ", length * "^")
    print(f" {line_index + 2} | {lines[line_index + 1]}")

    return str()

def read_until(break_char_set, text, pos):
    old_pos = pos
    while text[pos] not in break_char_set:
        pos += 1
    return text[old_pos:pos], pos

def read_while(allowed_char_set, text, pos):
    old_pos = pos
    while text[pos] in allowed_char_set:
        pos += 1
    return text[old_pos:pos], pos

def read_whitespace(text, pos):
    _, pos = read_while(" \t\n", text, pos)
    if text[pos] == "%" and text[pos + 1] == "%":
        _, pos = read_until("\n", text, pos)
        pos = read_whitespace(text, pos)
    return pos

def read_identifier(text, pos):
    out, pos = read_while("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789$", text, pos)
    if not out:
        print_text_position(text, pos, 1)
        raise ValueError(f"Expected identifier, got '{text[pos]}'.")
    return out, pos

def read_string(string, text, pos):
    if text[pos:pos + len(string)] != string:
        print_text_position(text, pos, len(string))
        raise ValueError(f"Expected '{string}', got '{text[pos:pos+len(string)]}'.")

    return pos + len(string)

def read_char(allowed_character_set, text, pos):
    if text[pos] not in allowed_character_set:
        print_text_position(text, pos, 1)
        raise ValueError(f"Expected character of '{allowed_character_set}', got '{text[pos]}'.")

    return pos + 1

def read_optional_string(string, text, pos):
    if text[pos:pos + len(string)] == string:
        return pos + len(string)
    return pos

read_optional_char = read_optional_string

def read_value(text, pos):
    match text[pos]:
        case '[' | '{':
            return read_list(text, pos)
        case '<':
            return read_binary(text, pos)
        case '#':
            return read_record(text, pos)
        case "'":
            return read_identifier_literal(text, pos)
        case '"':
            return read_string_literal(text, pos)
    return read_identifier(text, pos)

def read_list(text, pos):
    pos = read_char("[{", text, pos)
    pos = read_whitespace(text, pos)

    values = []
    while text[pos] not in "]}":
        value, pos = read_value(text, pos)
        pos = read_whitespace(text, pos)
        pos = read_optional_char(",", text, pos)
        pos = read_whitespace(text, pos)
        
        values.append(value)
    pos = read_char("]}", text, pos)

    return values, pos

def read_string_literal(text, pos):
    pos = read_char('"', text, pos)
    value, pos = read_until('"', text, pos)
    pos = read_char('"', text, pos)
    return value, pos    

def read_binary(text, pos):
    pos = read_string("<<", text, pos)
    pos = read_whitespace(text, pos)
    match text[pos]:
        case '"':
            value, pos = read_string_literal(text, pos)
        case '>':
            value = ""
        case c:
            print_text_position(text, pos, 1)
            raise ValueError(f"Expected string literal or '>>', got '{text[pos]}' while parsing binary.")
    pos = read_whitespace(text, pos)
    pos = read_string(">>", text, pos)
    return value, pos

def read_identifier_literal(text, pos):
    pos = read_char("'", text, pos)
    value, pos = read_until("'", text, pos)
    pos = read_char("'", text, pos)
    return value, pos

def read_record(text, pos):
    output = dict()

    pos = read_char("#", text, pos)
    name, pos = read_identifier(text, pos)
    pos = read_char("{", text, pos)

    pos = read_whitespace(text, pos)
    while text[pos] != "}":
        key, pos = read_identifier(text, pos)
        assert len(key) > 0, print_text_position(text, pos, 1)
        pos = read_whitespace(text, pos)
        pos = read_char('=', text, pos)
        pos = read_whitespace(text, pos)
        value, pos = read_value(text, pos)
        pos = read_whitespace(text, pos)
        pos = read_optional_char(",", text, pos)
        pos = read_whitespace(text, pos)

        output[key] = value

    pos = read_char("}", text, pos)

    # record name is not stored (not needed in most cases)
    return output, pos

def read_start_xml(text, pos):
    pos = read_whitespace(text, pos)
    name, pos = read_identifier(text, pos)
    pos = read_char(",", text, pos)
    pos = read_whitespace(text, pos)
    elem, pos = read_record(text, pos)
    return name, elem

def read_start_spec(text):
    xml_elements = dict()

    pos = 0
    while True:
        pos = text.find("-xml(", pos + 1)
        if pos == -1:
            break

        name, elem = read_start_xml(text, pos + len("-xml("))
        xml_elements[name] = elem
    return xml_elements

try:
    call, filename = sys.argv
except ValueError:
    print(f"Usage: {sys.argv[0]} [xml_codec.spec file]")
    exit(1)

xmpp_codec = open(filename).read()
out = read_start_spec(xmpp_codec)

print(json.dumps(out))
