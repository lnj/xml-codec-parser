# python-based erlang fastxml xml codec parser

This is a parser for XML specification files for the erlang fast_xml parsing library.
It allows to use the XML specification data for other use-cases, e.g. generating parsing code in other languages.

A good example is the `xmpp_codec.spec` file from ejabberd's XMPP library *p1-xmpp*:
```bash
wget https://github.com/processone/xmpp/raw/master/specs/xmpp_codec.spec
./erl-xml-codec-parser.py xmpp_codec.spec
```

This outputs the data from it in JSON.

